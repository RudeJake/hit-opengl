using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;


namespace OpenGL
{
    class cOGL
    {
        Control p;
        int Width;
        int Height;
        
        

        public cOGL(Control pb)
        {
            p=pb;
            Width = p.Width;
            Height = p.Height; 
            InitializeGL();
        }

        ~cOGL()
        {
            WGL.wglDeleteContext(m_uint_RC);
        }

		uint m_uint_HWND = 0;
        public uint HWND
		{
			get{ return m_uint_HWND; }
		}
		
        uint m_uint_DC   = 0;

        public uint DC
		{
			get{ return m_uint_DC;}
		}
		uint m_uint_RC   = 0;

        public uint RC
		{
			get{ return m_uint_RC; }
		}

        protected void DrawAll()
        {
            GL.glDisable(GL.GL_LIGHTING);
            //axes
            GL.glBegin(GL.GL_LINES);
            //x  RED
            GL.glColor3f(1.0f, 0.0f, 0.0f);
            GL.glVertex3f(-3.0f, 0.0f, 0.0f);
            GL.glVertex3f(3.0f, 0.0f, 0.0f);
            //y  GREEN 
            GL.glColor3f(0.0f, 2.0f, 0.0f);
            GL.glVertex3f(0.0f, -3.0f, 0.0f);
            GL.glVertex3f(0.0f, 3.0f, 0.0f);
            //z  BLUE
            GL.glColor3f(0.0f, 0.0f, 2.0f);
            GL.glVertex3f(0.0f, 0.0f, -3.0f);
            GL.glVertex3f(0.0f, 0.0f, 3.0f);

            GL.glEnd();

            //textering
            GL.glEnable(GL.GL_TEXTURE_2D);
            GL.glBindTexture(GL.GL_TEXTURE_2D, textures[0]);
            GL.glDisable(GL.GL_LIGHTING);

            //light
            GL.glEnable(GL.GL_LIGHTING);
            GL.glEnable(GL.GL_LIGHT0);
            GL.glEnable(GL.GL_COLOR_MATERIAL);

            // cube

            GL.glBegin(GL.GL_QUADS);


            //1
            GL.glColor3f(0.7f, 0.46f, 0.07f);
            GL.glNormal3f(0, 0, -1);

            GL.glVertex3f(-0.5f, -0.5f, 0.0f);
            GL.glVertex3f(-0.5f, 0.5f, 0.0f);
            GL.glVertex3f(0.5f, 0.5f, 0.0f);
            GL.glVertex3f(0.5f, -0.5f, 0.0f);


            //reset color
            GL.glColor3f(1.0f, 1.0f, 1.0f);

            //2
            GL.glNormal3f(-1, 0, 0);
            
            GL.glTexCoord2f(0.0f, 0.0f);
            GL.glVertex3f(-0.5f, -0.5f, 1.0f);

            GL.glTexCoord2f(0.25f, 0.0f);
            GL.glVertex3f(-0.5f, 0.5f, 1.0f);


            GL.glTexCoord2f(0.25f, 1.0f);
            GL.glVertex3f(-0.5f, 0.5f, 0.0f);


            GL.glTexCoord2f(0.0f, 1.0f);
            GL.glVertex3f(-0.5f, -0.5f, 0.0f);


            //3
            GL.glNormal3f(0, -1, 0);
            GL.glTexCoord2f(0.25f, 0.0f);
            GL.glVertex3f(0.5f, -0.5f, 1.0f);

            GL.glTexCoord2f(0.5f, 0.0f);
            GL.glVertex3f(-0.5f, -0.5f, 1.0f);

            GL.glTexCoord2f(0.5f, 1.0f);
            GL.glVertex3f(-0.5f, -0.5f, 0.0f);

            GL.glTexCoord2f(0.25f, 1.0f);
            GL.glVertex3f(0.5f, -0.5f, 0.0f);



            //4

            GL.glNormal3f(1, 0, 0);
            GL.glTexCoord2f(0.5f, 0.0f);
            GL.glVertex3f(0.5f, 0.5f, 1.0f);

            GL.glTexCoord2f(0.75f, 0.0f);
            GL.glVertex3f(0.5f, -0.5f, 1.0f);

            GL.glTexCoord2f(0.75f, 1.0f);
            GL.glVertex3f(0.5f, -0.5f, 0.0f);

            GL.glTexCoord2f(0.5f, 1.0f);
            GL.glVertex3f(0.5f, 0.5f, 0.0f);

            //5
            GL.glNormal3f(0, 1, 0);
            GL.glTexCoord2f(0.75f, 0.0f);
            GL.glVertex3f(-0.5f, 0.5f, 1.0f);

            GL.glTexCoord2f(1.0f, 0.0f);
            GL.glVertex3f(0.5f, 0.5f, 1.0f);

            GL.glTexCoord2f(1.0f, 1.0f);
            GL.glVertex3f(0.5f, 0.5f, 0.0f);

            GL.glTexCoord2f(0.75f, 1.0f);
            GL.glVertex3f(-0.5f, 0.5f, 0.0f);


            //6

            GL.glColor3f(0.7f, 0.46f, 0.07f);
            GL.glNormal3f(0, 0, 1);

            GL.glVertex3f(0.5f, 0.5f, 1.0f);

            GL.glVertex3f(-0.5f, 0.5f, 1.0f);

            GL.glVertex3f(-0.5f, -0.5f, 1.0f);

            GL.glVertex3f(0.5f, -0.5f, 1.0f);

            GL.glEnd();


            //roof
            GL.glEnable(GL.GL_NORMALIZE);
            GL.glBegin(GL.GL_TRIANGLES);

            GL.glColor3f(0.7f, 0.46f, 0.07f);
            //1



            GL.glVertex3f(0.5f, 0.5f, 1.0f);
            GL.glVertex3f(-0.5f, 0.5f, 1.0f);
            GL.glVertex3f(0.0f, 0.0f, 1.6f);
            calcNormOnTriangle(new float [] { 0.5f,0.5f,1.0f}, new float[] { -0.5f,0.5f,1.0f}, new float[] { 0.0f,0.0f,1.6f});

            //2
            GL.glVertex3f(0.5f, 0.5f, 1.0f);
            GL.glVertex3f(0.5f, -0.5f, 1.0f);
            GL.glVertex3f(0.0f, 0.0f, 1.6f);

            calcNormOnTriangle(new float[] { 0.5f, 0.5f, 1.0f }, new float[] { 0.5f, -0.5f, 1.0f }, new float[] { 0.0f, 0.0f, 1.6f });

            //3
            GL.glVertex3f(-0.5f, -0.5f, 1.0f);
            GL.glVertex3f(0.5f, -0.5f, 1.0f);
            GL.glVertex3f(0.0f, 0.0f, 1.6f);
            calcNormOnTriangle(new float[] { -0.5f, -0.5f, 1.0f }, new float[] { 0.5f, -0.5f, 1.0f }, new float[] { 0.0f, 0.0f, 1.6f });

            //4
            GL.glVertex3f(-0.5f, -0.5f, 1.0f);
            GL.glVertex3f(-0.5f, 0.5f, 1.0f);
            GL.glVertex3f(0.0f, 0.0f, 1.6f);
            calcNormOnTriangle(new float[] { -0.5f, -0.5f, 1.0f }, new float[] { -0.5f, 0.5f, 1.0f }, new float[] { 0.0f, 0.0f, 1.6f });

            GL.glEnd();


            //cylinder

            

            GLUquadric obj;
            obj = GLU.gluNewQuadric();

            GL.glColor3f(0.6f, 0.36f, 0.07f);
            GL.glTranslatef(0, 0, -0.5f);

            GLU.gluCylinder(obj, 0.25f, 0.25, 0.5, 16, 16);



            //cylinder cap
            GL.glDisable(GL.GL_LIGHTING);
            GL.glColor3f(0.4f, 0.15f, 0);
            GLU.gluDisk(obj, 0, 0.25f, 16, 16);

            //reset
            GLU.gluDeleteQuadric(obj);
            GL.glTranslatef(0, 0, 0.5f);
            GL.glDisable(GL.GL_LIGHTING);

        }


        float angle = 0.0f;
        public void Draw()
        {
            if (m_uint_DC == 0 || m_uint_RC == 0)
                return;

            GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
            GL.glLoadIdentity();

            GL.glTranslatef(0.0f, 0.0f, -6.0f);                     // Translate 6 Units Into The Screen
            //GL.glTranslatef(0.0f, 1.5f, 0.0f);

            GL.glRotatef(115, 1.0f, 0.0f, 0.0f);
            GL.glRotatef(15, 0.0f, 0.0f, 1.0f);

            angle -= 5.0f;
            GL.glRotatef(angle, 0.0f, 0.0f, 1.0f);

            DrawAll();


            DrawAll();


            GL.glFlush();

            WGL.wglSwapBuffers(m_uint_DC);

        }

		protected virtual void InitializeGL()
		{
            // connection between our panel and OpenGL
			m_uint_HWND = (uint)p.Handle.ToInt32();
			m_uint_DC   = WGL.GetDC(m_uint_HWND);

            // Not doing the following WGL.wglSwapBuffers() on the DC will
			// result in a failure to subsequently create the RC.
			WGL.wglSwapBuffers(m_uint_DC); // example not flick, two buffer for two frame 
                                           //one visible one not // we work we the invisible and see visible when we finish swap happens

			WGL.PIXELFORMATDESCRIPTOR pfd = new WGL.PIXELFORMATDESCRIPTOR();
			WGL.ZeroPixelDescriptor(ref pfd);
			pfd.nVersion        = 1; 
			pfd.dwFlags         = (WGL.PFD_DRAW_TO_WINDOW |  WGL.PFD_SUPPORT_OPENGL |  WGL.PFD_DOUBLEBUFFER); 
			pfd.iPixelType      = (byte)(WGL.PFD_TYPE_RGBA);
			pfd.cColorBits      = 32;
			pfd.cDepthBits      = 32;
			pfd.iLayerType      = (byte)(WGL.PFD_MAIN_PLANE);

			int pixelFormatIndex = 0;
			pixelFormatIndex = WGL.ChoosePixelFormat(m_uint_DC, ref pfd);
			if(pixelFormatIndex == 0)
			{
				MessageBox.Show("Unable to retrieve pixel format");
				return;
			}

			if(WGL.SetPixelFormat(m_uint_DC,pixelFormatIndex,ref pfd) == 0)
			{
				MessageBox.Show("Unable to set pixel format");
				return;
			}
			//Create rendering context
			m_uint_RC = WGL.wglCreateContext(m_uint_DC);
			if(m_uint_RC == 0)
			{
				MessageBox.Show("Unable to get rendering context");
				return;
			}
			if(WGL.wglMakeCurrent(m_uint_DC,m_uint_RC) == 0)
			{
				MessageBox.Show("Unable to make rendering context current");
				return;
			}


            initRenderingGL(); //
        }

        public void OnResize()
        {
            Width = p.Width;
            Height = p.Height;

            //!!!!!!!
            GL.glViewport(0, 0, Width, Height);
            //!!!!!!!
            
            initRenderingGL();
            Draw();
        }



        protected virtual void initRenderingGL()
		{
			if(m_uint_DC == 0 || m_uint_RC == 0)
				return;
			if(this.Width == 0 || this.Height == 0)
				return;
            GL.glEnable(GL.GL_DEPTH_TEST); // zBufferAlgorithm for z axis -> 3D also will no reproduce a hide graphic part that is not projected like the back part of a cube.
            //GL.glDisable(GL.GL_DEPTH_TEST); //visual example
            GL.glDepthFunc(GL.GL_LEQUAL); // kind of properties for display decission 

            GL.glViewport(0, 0, this.Width, this.Height); //begin clearing
            GL.glClearColor(0, 0, 0, 0); //begin clearing // 4 arguments, 3 RGB 1 contrast
			GL.glMatrixMode ( GL.GL_PROJECTION ); // unit matrix 
			GL.glLoadIdentity();
			GLU.gluPerspective( 45.0,((double)Width) / Height, 1.0,1000.0); // for projection
			GL.glMatrixMode ( GL.GL_MODELVIEW ); // 3D transformation for projection, from 3D graphic -> 2D display (screen)
			GL.glLoadIdentity();

            InitTexture("dreidel_tex.bmp");
        }


        public uint[] textures;
        public void InitTexture(string imageBMPfile)
        {
            GL.glEnable(GL.GL_TEXTURE_2D);

            textures = new uint[1];		// storage for texture

            Bitmap image = new Bitmap(imageBMPfile);
            image.RotateFlip(RotateFlipType.RotateNoneFlipY); //Y axis in Windows is directed downwards, while in OpenGL-upwards
            System.Drawing.Imaging.BitmapData bitmapdata;
            Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);

            bitmapdata = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            GL.glGenTextures(1, textures);
            GL.glBindTexture(GL.GL_TEXTURE_2D, textures[0]);
            //  VN-in order to use System.Drawing.Imaging.BitmapData Scan0 I've added overloaded version to
            //  OpenGL.cs
            //  [DllImport(GL_DLL, EntryPoint = "glTexImage2D")]
            //  public static extern void glTexImage2D(uint target, int level, int internalformat, int width, int height, int border, uint format, uint type, IntPtr pixels);
            GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, (int)GL.GL_RGB8, image.Width, image.Height,
                0, GL.GL_BGR_EXT, GL.GL_UNSIGNED_byte, bitmapdata.Scan0);
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, (int)GL.GL_LINEAR);		// Linear Filtering
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, (int)GL.GL_LINEAR);		// Linear Filtering

            image.UnlockBits(bitmapdata);
            image.Dispose();
        }

        private void calcNormOnTriangle(float[] P1, float[] P2, float[] P3)
        {
            // V = P2 - P1 , W = P3 - P1
            //Nx = (Vy * Wz) - (Vz * Wy)
            //Ny = (Vz * Wx) - (Vx * Wz)
            //Nz = (Vx * Wy) - (Vy * Wx)

            float Nx = ((P2[1] - P1[1]) * (P3[2] - P1[2])) - ((P2[2] - P1[2]) * (P3[1] - P1[1]));
            float Ny = ((P2[2] - P1[2]) * (P3[0] - P1[0])) - ((P2[0] - P1[0]) * (P3[2] - P1[2]));
            float Nz = ((P2[0] - P1[0]) * (P3[1] - P1[1])) - ((P2[1] - P1[1]) * (P3[0] - P1[0]));
            GL.glNormal3f(Nx, Ny, Nz);

        }



    }
}
